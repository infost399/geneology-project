﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeneologyProject.Models
{
    public class AdminDashboard
    {
        public List<person> People { get; set; }

        public List<cemetery> Cemeteries { get; set; }

        public List<location> Locations { get; set; }

        public List<wedding> Weddings { get; set; }

        public List<cart> Orders { get; set; }

        public List<user> OrderUsers { get; set; }
    }
}