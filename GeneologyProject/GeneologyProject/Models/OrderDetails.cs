﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeneologyProject.Models
{
    public class OrderDetails
    {
        public cart cart { get; set; }

        public List<cart_items> items { get; set; }

        public user user { get; set; }

        public List<item_types> types { get; set; }

        public List<person> people { get; set; }
    }
}