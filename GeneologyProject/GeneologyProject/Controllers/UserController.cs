﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GeneologyProject.Models;
using BCrypt.Net;

namespace GeneologyProject.Controllers
{
    public class UserController : Controller
    {
        private GeneologyDB db = new GeneologyDB();

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(user newUser)
        {
            if (!db.users.Any(x => x.email == newUser.email))
            {
                var userGUID = Guid.NewGuid();
                var password = newUser.hash;

                newUser.user_guid = userGUID;
                newUser.hash = BCrypt.Net.BCrypt.HashPassword(password);
                newUser.ip_address = Request.UserHostAddress;

                db.users.Add(newUser);
                db.SaveChanges();
            }
            else
            {
                ModelState.AddModelError("email", "this email is already in use");
                return View();
            }

            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(user login)
        {
            var user = db.users.FirstOrDefault(x => x.email == login.email);
            var loginIP = Request.UserHostAddress;

            if (user != null)
            {
                if (BCrypt.Net.BCrypt.Verify(login.hash, user.hash))
                {
                    //login successfull
                    Session["AuthID"] = Guid.NewGuid().ToString();
                    Session["AuthType"] = "User";
                    Session["User"] = user;

                    var authID = new HttpCookie("AuthID");
                    authID.Value = Session["AuthID"].ToString();
                    Response.Cookies.Add(authID);

                    var authType = new HttpCookie("AuthType");
                    authType.Value = "User";
                    Response.Cookies.Add(authType);

                    var cart = db.carts.FirstOrDefault(x => x.user_guid == user.user_guid);
                    if (cart != null)
                    {
                        var items = db.cart_items.Where(x => x.cart_guid == cart.cart_guid).ToList();
                        if (items.Count > 0)
                        {
                            Session["Cart"] = cart;
                        }
                    }

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("hash", "Incorrect email or password.");
                    return View(login);
                }
            }
            else
            {
                ModelState.AddModelError("hash", "Incorrect email or password.");
                return View(login);
            }
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(user editUser)
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            Response.Cookies.Clear();

            return RedirectToAction("Index", "Home");
        }
    }
}