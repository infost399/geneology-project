﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GeneologyProject.Models;
using BCrypt.Net;

namespace GeneologyProject.Controllers
{
    public class AdminController : Controller
    {
        private GeneologyDB db = new GeneologyDB();

        private bool CheckAuthentication()
        {
            try
            {
                if (Request.Cookies["AuthID"].Value == Session["AuthID"].ToString() && Request.Cookies["AuthType"].Value == Session["AuthType"].ToString() && Session["AuthType"].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public ActionResult Dashboard()
        {
            if (CheckAuthentication())
            {
                AdminDashboard dashboard = new AdminDashboard();
                var weekBefore = DateTime.Now.AddDays(-7);

                var people = db.people.Where(x => x.date_modified != null && x.date_modified != "").ToList();
                List<person> dashboardPeople = new List<person>();
                foreach (var person in people)
                {
                    var date = Convert.ToDateTime(person.date_modified);
                    if (date > weekBefore)
                    {
                        dashboardPeople.Add(person);
                    }
                }
                dashboard.People = dashboardPeople.OrderByDescending(x => x.date_modified).ToList();

                var cemeteries = db.cemeteries.Where(x => x.date_modified != null && x.date_modified != "").ToList();
                List<cemetery> dashboardCemeteries = new List<cemetery>();
                foreach (var cemetery in cemeteries)
                {
                    var date = Convert.ToDateTime(cemetery.date_modified);
                    if (date > weekBefore)
                    {
                        dashboardCemeteries.Add(cemetery);
                    }
                }
                dashboard.Cemeteries = dashboardCemeteries.OrderByDescending(x => x.date_modified).ToList();

                var locations = db.locations.Where(x => x.date_modified != null && x.date_modified != "").ToList();
                List<location> dashboardLocations = new List<location>();
                foreach (var location in locations)
                {
                    var date = Convert.ToDateTime(location.date_modified);
                    if (date > weekBefore)
                    {
                        dashboardLocations.Add(location);
                    }
                }
                dashboard.Locations = dashboardLocations.OrderByDescending(x => x.date_modified).ToList();

                var weddings = db.weddings.Where(x => x.date_modified != null && x.date_modified != "").ToList();
                List<wedding> dashboardWeddings = new List<wedding>();
                foreach (var wedding in weddings)
                {
                    var date = Convert.ToDateTime(wedding.date_modified);
                    if (date > weekBefore)
                    {
                        dashboardWeddings.Add(wedding);
                    }
                }
                dashboard.Weddings = dashboardWeddings.OrderByDescending(x => x.date_modified).ToList();

                var orders = db.carts.Where(x => x.order_placed == true && x.order_closed == false).OrderBy(x => x.order_date).ToList();
                dashboard.Orders = orders.ToList();

                List<user> users = new List<user>();
                foreach(var order in orders)
                {
                    var user = db.users.FirstOrDefault(x => x.user_guid == order.user_guid);
                    if(!users.Any(x => x.user_guid == user.user_guid))
                    {
                        users.Add(user);
                    }
                }
                dashboard.OrderUsers = users.ToList();

                return View(dashboard);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Orders()
        {
            if (CheckAuthentication())
            {
                var orders = db.carts.Where(x => x.order_placed == true && x.order_closed == false).OrderBy(x => x.order_date).ToList();

                var users = db.users.ToList();
                ViewBag.Users = users.ToList();

                return View(orders);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult OrderDetails(string cartGUID)
        {
            if (CheckAuthentication())
            {
                var cart_guid = Guid.Parse(cartGUID);
                var cart = db.carts.FirstOrDefault(x => x.cart_guid == cart_guid);

                var user = db.users.FirstOrDefault(x => x.user_guid == cart.user_guid);

                var cartItems = db.cart_items.Where(x => x.cart_guid == cart.cart_guid).ToList();

                List<person> people = new List<person>();
                foreach(var item in cartItems)
                {
                    var person = db.people.FirstOrDefault(x => x.person_guid == item.person_guid);
                    if(!people.Any(x => x.person_guid == person.person_guid))
                    {
                        people.Add(person);
                    }
                }

                var types = db.item_types.ToList();

                OrderDetails details = new OrderDetails
                {
                    cart = cart,
                    items = cartItems,
                    user = user,
                    types = types,
                    people = people,
                };

                return View(details);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        #region ---- People Data ----
        public ActionResult PersonList(string filter)
        {
            if (CheckAuthentication())
            {
                List<person> people = new List<person>();

                List<person> allPeople = db.people.OrderBy(x => x.last_name).ThenBy(x => x.first_name).ToList();
                ViewBag.AllPeople = allPeople.ToList();

                if (String.IsNullOrWhiteSpace(filter))
                {
                    people = allPeople.OrderBy(x => x.last_name).ThenBy(x => x.first_name).ToList();
                    ViewBag.People = people.ToList();
                    ViewBag.Filter = "";
                }
                else
                {
                    people = allPeople.Where(x => x.last_name.StartsWith(filter)).OrderBy(x => x.last_name).ThenBy(x => x.first_name).ToList();
                    ViewBag.People = people.ToList();
                    ViewBag.Filter = filter;
                }


                var weddings = db.weddings.Distinct().ToList();
                var weddings2 = weddings.ToList();
                foreach (var wedding in weddings)
                {
                    if (weddings.Any(x => x.person1_id == wedding.person2_id && x.person2_id == wedding.person1_id))
                    {
                        weddings2.Remove(wedding);
                    }
                }

                ViewBag.Weddings = weddings2.OrderBy(x => x.wedding_date).ToList();

                List<cemetery> cemeteries = db.cemeteries.OrderBy(x => x.cemetery_name).ToList();
                ViewBag.Cemeteries = cemeteries.ToList();

                List<location> locations = db.locations.OrderBy(x => x.country_code).ThenBy(x => x.state_region).ThenBy(x => x.county_province).ThenBy(x => x.township).ThenBy(x => x.city_village).ToList();
                ViewBag.Locations = locations.ToList();

                List<filter> filters = db.filters.OrderBy(x => x.filter1).ToList();
                ViewBag.Filters = filters.ToList();


                return View(people);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult EditPerson(Guid personGUID)
        {
            if (CheckAuthentication())
            {
                Session["personGUID"] = personGUID;

                var original = db.people.First(x => x.person_guid == personGUID);

                Session["personID"] = original.person_id;

                ViewBag.Locations = getLocations();

                List<SelectListItem> parentsList = new List<SelectListItem>();

                var people = db.people.OrderBy(x => x.first_name).ToList();

                foreach (var person in people)
                {
                    parentsList.Add(new SelectListItem { Value = person.person_id.ToString(), Text = person.first_name + " " + person.middle_name + " " + person.last_name });
                }

                ViewBag.Parents = parentsList;

                List<SelectListItem> cemeteryList = new List<SelectListItem>();

                var cemeteries = db.cemeteries.OrderBy(x => x.cemetery_name).ToList();

                foreach (var cemetery in cemeteries)
                {
                    cemeteryList.Add(new SelectListItem { Value = cemetery.cemetery_id.ToString(), Text = cemetery.cemetery_name });
                }

                ViewBag.Cemeteries = cemeteryList;

                return View(original);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult EditPerson(person editPerson)
        {
            var original = db.people.First(x => x.person_guid == editPerson.person_guid);

            original.first_name = editPerson.first_name;
            original.last_name = editPerson.last_name;
            original.middle_name = editPerson.middle_name;
            original.father_id = editPerson.father_id;
            original.mother_id = editPerson.mother_id;
            original.nickname = editPerson.nickname;
            original.prefix = editPerson.prefix;
            original.gender = editPerson.gender;
            original.birth_date = editPerson.birth_date;
            original.birth_location_id = editPerson.birth_location_id;
            original.death_date = editPerson.death_date;
            original.death_place = editPerson.death_place;
            original.cause_of_death = editPerson.cause_of_death;
            original.death_location_id = editPerson.death_location_id;
            original.cemetery_id = editPerson.cemetery_id;
            original.date_modified = DateTime.Now.ToShortDateString();

            db.SaveChanges();

            return RedirectToAction("PersonList");
        }

        public ActionResult CreatePerson()
        {
            if (CheckAuthentication())
            {
                ViewBag.Locations = getLocations();

                List<SelectListItem> parentsList = new List<SelectListItem>();

                var people = db.people.OrderBy(x => x.first_name).ToList();

                foreach (var person in people)
                {
                    parentsList.Add(new SelectListItem { Value = person.person_id.ToString(), Text = person.first_name + " " + person.middle_name + " " + person.last_name });
                }

                ViewBag.Parents = parentsList;

                List<SelectListItem> cemeteryList = new List<SelectListItem>();

                var cemeteries = db.cemeteries.OrderBy(x => x.cemetery_name).ToList();

                foreach (var cemetery in cemeteries)
                {
                    cemeteryList.Add(new SelectListItem { Value = cemetery.cemetery_id.ToString(), Text = cemetery.cemetery_name });
                }

                ViewBag.Cemeteries = cemeteryList;

                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult CreatePerson(person newPerson)
        {
            var maxPersonID = db.people.Max(x => x.person_id) + 1;
            var newGuid = Guid.NewGuid();

            newPerson.person_guid = newGuid;
            newPerson.person_id = maxPersonID;

            db.people.Add(newPerson);
            db.SaveChanges();

            return RedirectToAction("PersonList");

        }

        #endregion
        #region ---- Location Data ----
        public ActionResult LocationList()
        {
            if (CheckAuthentication())
            {
                var locations = db.locations.OrderBy(x => x.country_code).ThenBy(x => x.state_region).ThenBy(x => x.county_province).ThenBy(x => x.township).ThenBy(x => x.city_village).ToList();

                return View(locations);
            }
            else
            {
                return RedirectToAction("Login");
            }

        }

        public ActionResult EditLocation(Guid locationGUID)
        {
            if (CheckAuthentication())
            {

                var location = db.locations.First(x => x.location_guid == locationGUID);

                return View(location);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult EditLocation(location editLocation)
        {
            var original = db.locations.First(x => x.location_guid == editLocation.location_guid);

            original.country_code = editLocation.country_code;
            original.state_region = editLocation.state_region;
            original.county_province = editLocation.county_province;
            original.township = editLocation.township;
            original.city_village = editLocation.city_village;
            original.date_modified = DateTime.Now.ToShortDateString();

            db.SaveChanges();

            return RedirectToAction("LocationList");
        }

        public ActionResult CreateLocation()
        {
            if (CheckAuthentication())
            {
                ViewBag.CountryCodes = db.locations.Select(x => x.country_code).Distinct().ToList();

                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult CreateLocation(location newLocation)
        {
            var locationID = db.locations.Max(x => x.location_id) + 1;
            var locationGUID = Guid.NewGuid();

            newLocation.location_id = locationID;
            newLocation.location_guid = locationGUID;

            db.locations.Add(newLocation);
            db.SaveChanges();

            return RedirectToAction("LocationList");
        }
        #endregion
        #region ---- Cemetery Data ----
        public ActionResult CemeteryList()
        {
            if (CheckAuthentication())
            {
                var cemeteries = db.cemeteries.OrderBy(x => x.cemetery_name).ToList();

                var locations = db.locations.OrderBy(x => x.country_code).ThenBy(x => x.state_region).ThenBy(x => x.county_province).ThenBy(x => x.township).ThenBy(x => x.city_village).ToList();

                ViewBag.Locations = locations.ToList();

                return View(cemeteries);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult EditCemetery(Guid cemeteryGUID)
        {
            if (CheckAuthentication())
            {
                var cemetery = db.cemeteries.First(x => x.cemetery_guid == cemeteryGUID);

                ViewBag.Locations = getLocations();

                return View(cemetery);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult EditCemetery(cemetery editCemetery)
        {
            var original = db.cemeteries.First(x => x.cemetery_guid == editCemetery.cemetery_guid);

            original.cemetery_name = editCemetery.cemetery_name;
            original.cemetery_location_id = editCemetery.cemetery_location_id;
            original.date_modified = DateTime.Now.ToShortDateString();

            db.SaveChanges();

            return RedirectToAction("CemeteryList");
        }

        public ActionResult CreateCemetery()
        {
            if (CheckAuthentication())
            {
                ViewBag.Locations = getLocations();

                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult CreateCemetery(cemetery newCemetery)
        {
            var maxCemeteryID = db.cemeteries.Max(x => x.cemetery_id) + 1;
            var cemeteryGUID = Guid.NewGuid();

            newCemetery.cemetery_id = maxCemeteryID;
            newCemetery.cemetery_guid = cemeteryGUID;

            db.cemeteries.Add(newCemetery);
            db.SaveChanges();

            return RedirectToAction("CemeterList");
        }
        #endregion
        #region ---- Wedding Data ----
        public ActionResult EditMarriage(Guid weddingGUID)
        {
            if (CheckAuthentication())
            {
                var wedding = db.weddings.FirstOrDefault(x => x.wedding_guid == weddingGUID);

                ViewBag.Locations = getLocations();

                List<SelectListItem> personList = new List<SelectListItem>();
                var people = db.people.OrderBy(x => x.first_name).ToList();

                foreach (var person in people)
                {
                    personList.Add(new SelectListItem { Value = person.person_id.ToString(), Text = person.first_name + " " + person.middle_name + " " + person.last_name });
                }

                ViewBag.People = personList;

                return View(wedding);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult EditMarriage(wedding editWedding)
        {
            var original = db.weddings.FirstOrDefault(x => x.wedding_guid == editWedding.wedding_guid);

            original.person1_id = editWedding.person1_id;
            original.person2_id = editWedding.person2_id;
            original.wedding_date = editWedding.wedding_date;
            original.wedding_place = editWedding.wedding_place;
            original.wedding_location_id = editWedding.wedding_location_id;
            original.date_modified = DateTime.Now.ToShortDateString();

            db.SaveChanges();

            return RedirectToAction("PersonList");
        }

        public ActionResult CreateMarriage()
        {
            if (CheckAuthentication())
            {
                ViewBag.Locations = getLocations();

                List<SelectListItem> personList = new List<SelectListItem>();
                var people = db.people.OrderBy(x => x.first_name).ToList();

                foreach (var person in people)
                {
                    personList.Add(new SelectListItem { Value = person.person_id.ToString(), Text = person.first_name + " " + person.middle_name + " " + person.last_name });
                }

                ViewBag.People = personList;

                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult CreateMarriage(wedding newWedding)
        {
            var weddingGUID = Guid.NewGuid();

            newWedding.wedding_guid = weddingGUID;

            db.weddings.Add(newWedding);
            db.SaveChanges();

            return RedirectToAction("PersonList");
        }
        #endregion
        #region ---- Login ----
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(admin login)
        {
            var admin = db.admins.FirstOrDefault(x => x.username == login.username);

            if (admin != null)
            {
                if (BCrypt.Net.BCrypt.Verify(login.hash, admin.hash))
                {
                    Session["AuthID"] = Guid.NewGuid().ToString();
                    Session["AuthType"] = "Admin";

                    var authID = new HttpCookie("AuthID");
                    authID.Value = Session["AuthID"].ToString();
                    Response.Cookies.Add(authID);

                    var authType = new HttpCookie("AuthType");
                    authType.Value = "Admin";
                    Response.Cookies.Add(authType);

                    return RedirectToAction("Dashboard");
                }
                else
                {
                    ModelState.AddModelError("hash", "Incorrect username or password.");
                    return View(login);
                }
            }
            else
            {
                ModelState.AddModelError("hash", "Incorrect username or password.");
                return View(login);
            }
        }

        public ActionResult Register()
        {
            if (CheckAuthentication())
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult Register(admin newAdmin)
        {
            if (!db.admins.Any(x => x.username == newAdmin.username))
            {
                var adminGUID = Guid.NewGuid();
                var password = newAdmin.hash;

                newAdmin.admin_guid = adminGUID;
                newAdmin.hash = BCrypt.Net.BCrypt.HashPassword(password);

                db.admins.Add(newAdmin);
                db.SaveChanges();
            }
            else
            {
                ModelState.AddModelError("username", "this username is already in use");
                return View();
            }

            return RedirectToAction("Login");
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            Response.Cookies.Clear();

            return RedirectToAction("Login");

        }
        #endregion
        #region ---- Filters ----
        public ActionResult FilterList()
        {
            if (TempData["filter-error"] != null && !String.IsNullOrWhiteSpace(TempData["filter-error"].ToString()))
            {
                ViewBag.AlreadyExists = true;
                ViewBag.Message = TempData["filter-error"].ToString();
                ViewBag.FilterEntered = TempData["filter-endered"].ToString();
            }
            else
            {
                ViewBag.AlreadyExists = false;
                ViewBag.Message = "";
                ViewBag.FilterEntered = "";
            }

            var filters = db.filters.OrderBy(x => x.filter1).ToList();

            return View(filters);
        }

        public ActionResult AddFilter(string filter)
        {
            if (!db.filters.Any(x => x.filter1 == filter))
            {
                TempData["filter-error"] = "";
                TempData["filter-entered"] = "";

                db.filters.Add(new filter { filter1 = filter });
                db.SaveChanges();

                return RedirectToAction("FilterList");
            }
            else
            {
                TempData["filter-error"] = "Filter already exists";
                TempData["filter-entered"] = filter;
                return RedirectToAction("FilterList");
            }
        }

        public ActionResult DeleteFilter(string filter)
        {
            var filterToRemove = db.filters.FirstOrDefault(x => x.filter1 == filter);

            db.filters.Remove(filterToRemove);
            db.SaveChanges();

            return RedirectToAction("FilterList");

        }
        #endregion

        private List<SelectListItem> getLocations()
        {
            List<SelectListItem> locationsList = new List<SelectListItem>();

            var locations = db.locations.OrderBy(x => x.country_code).ThenBy(x => x.state_region).ThenBy(x => x.county_province).ThenBy(x => x.township).ThenBy(x => x.city_village).ToList();

            foreach (var location in locations)
            {
                var temp = "";
                if (!String.IsNullOrWhiteSpace(location.country_code))
                {
                    temp += location.country_code + ", ";
                }
                if (!String.IsNullOrWhiteSpace(location.state_region))
                {
                    temp += location.state_region + ", ";
                }
                if (!String.IsNullOrWhiteSpace(location.county_province))
                {
                    temp += location.county_province + " County, ";
                }
                if (!String.IsNullOrWhiteSpace(location.township))
                {
                    temp += location.township + ", ";
                }
                if (!String.IsNullOrWhiteSpace(location.city_village))
                {
                    temp += location.city_village + ", ";
                }
                temp = temp.Substring(0, temp.Length - 2);

                locationsList.Add(new SelectListItem { Value = location.location_id.ToString(), Text = temp });
            }

            return locationsList;
        }
    }
}