﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GeneologyProject.Models;

namespace GeneologyProject.Controllers
{
    public class HomeController : Controller
    {
        private GeneologyDB db = new GeneologyDB();

        public ActionResult Index()
        {
            if((user)Session["User"] != null)
            {
                CheckCart((user)Session["User"]);
            }

            var ip = db.users.Select(x => x.ip_address).ToList();
            ViewBag.IPs = ip.ToList();

            var filters = db.filters.OrderBy(x => x.filter1).ToList();
            ViewBag.Filters = filters.ToList();

            return View();
        }
        
        public ActionResult SearchResults(string filter)
        {
            if ((user)Session["User"] != null)
            {
                CheckCart((user)Session["User"]);
            }

            List<person> people = new List<person>();

            List<person> allPeople = db.people.OrderBy(x => x.last_name).ThenBy(x => x.first_name).ToList();
            ViewBag.AllPeople = allPeople.ToList();

            if (String.IsNullOrWhiteSpace(filter))
            {
                return RedirectToAction("Index");
            }
            else
            {
                var searchFilters = filter.Split(',').OrderBy(x => x).ToList();
                if(searchFilters.Count > 1)
                {
                    List<filter> searchedFilters = new List<filter>();
                    for (var counter = 0; counter < searchFilters.Count; counter++)
                    {
                        var searchResults = allPeople.Where(x => x.last_name.StartsWith(searchFilters[counter])).OrderBy(x => x.last_name).ThenBy(x => x.first_name).ToList();
                        foreach(var person in searchResults)
                        {
                            if(!people.Any(x => x.person_guid == person.person_guid))
                            {
                                people.Add(person);
                            }
                        }

                        searchedFilters.Add(new filter { filter1 = searchFilters[counter] });
                    }

                    ViewBag.SearchedFilters = searchedFilters.ToList();
                }
                else
                {
                    people = allPeople.Where(x => x.last_name.StartsWith(filter)).OrderBy(x => x.last_name).ThenBy(x => x.first_name).ToList();
                    ViewBag.SearchedFilters = new List<filter> { new filter { filter1 = filter } };
                }
                
                ViewBag.People = people.ToList();
            }


            var weddings = db.weddings.Distinct().ToList();
            var weddings2 = weddings.ToList();
            foreach (var wedding in weddings)
            {
                if (weddings.Any(x => x.person1_id == wedding.person2_id && x.person2_id == wedding.person1_id))
                {
                    weddings2.Remove(wedding);
                }
            }

            ViewBag.Weddings = weddings2.OrderBy(x => x.wedding_date).ToList();
                        
            return View(people);
        }

        public ActionResult AddToCart(String types, String ids)
        {
            var user = (user)Session["User"];
            var typesList = types.Split(',').ToList();
            var idsList = ids.Split(',').ToList();

            cart cart = null;

            if(user != null)
            {
                if(!db.carts.Any(x => x.user_guid == user.user_guid && x.order_placed == false))
                {
                    var maxCartID = 0;
                    try
                    {
                        maxCartID = Convert.ToInt32(db.carts.Max(x => x.cart_id));
                    }
                    catch
                    {
                        maxCartID = 0;
                    }

                    cart newCart = new cart
                    {
                        cart_guid = Guid.NewGuid(),
                        cart_id = maxCartID + 1,
                        user_guid = user.user_guid,
                        order_placed = false,
                        order_date = DateTime.Now.ToString("yyyy-MM-dd"),
                        order_closed = false,
                    };

                    db.carts.Add(newCart);
                    db.SaveChanges();

                    cart = newCart;
                }
                
                if(cart == null)
                {
                    cart = db.carts.FirstOrDefault(x => x.user_guid == user.user_guid && x.order_placed == false);
                }

                var numItems = typesList.Count();

                for (var y = 0; y < numItems; y++)
                {
                    var maxCartItemID = 0;
                    try
                    {
                        maxCartItemID = Convert.ToInt32(db.cart_items.Max(x => x.cart_item_id));
                    }
                    catch
                    {
                        maxCartItemID = 0;
                    }

                    var type = Convert.ToInt32(typesList[y]);
                    var typeGuid = db.item_types.FirstOrDefault(x => x.item_type_id == type).item_type_guid;

                    cart_items item = new cart_items()
                    {
                        cart_item_guid = Guid.NewGuid(),
                        cart_item_id = maxCartItemID + 1,
                        cart_guid = cart.cart_guid,
                        item_type_guid = typeGuid,
                        person_guid = Guid.Parse(idsList[y]),
                    };

                    db.cart_items.Add(item);
                }
                db.SaveChanges();
            }

            Session["Cart"] = cart;

            return RedirectToAction("Index");
        }

        public ActionResult RemoveFromCart(Guid userGuid, int cartItemID)
        {
            var cart = db.carts.FirstOrDefault(x => x.user_guid == userGuid && x.order_placed == false);
            if(cart != null)
            {
                var item = db.cart_items.FirstOrDefault(x => x.cart_guid == cart.cart_guid && x.cart_item_id == cartItemID);
                if(item != null)
                {
                    db.cart_items.Remove(item);
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Cart", new { userGuid });
        }

        public ActionResult Cart()
        {
            if ((user)Session["User"] != null)
            {
                CheckCart((user)Session["User"]);
            }

            var user = (user)Session["User"];
            var cart = db.carts.FirstOrDefault(x => x.user_guid == user.user_guid && x.order_placed == false);
            var itemTypes = db.item_types.ToList();
            var people = new List<person>();
            
            if(cart != null)
            {
                var items = db.cart_items.Where(x => x.cart_guid == cart.cart_guid).ToList();

                foreach(var item in items)
                {
                    var person = db.people.FirstOrDefault(x => x.person_guid == item.person_guid);
                    
                    people.Add(person);
                }

                ViewBag.People = people.ToList();
                ViewBag.ItemTypes = itemTypes.ToList();
                ViewBag.Cart = cart;

                return View(items);
            }

            return RedirectToAction("Index");
        }

        public ActionResult PlaceOrder(Guid cartGuid)
        {
            var cart = db.carts.FirstOrDefault(x => x.cart_guid == cartGuid);
            cart.order_placed = true;
            db.SaveChanges();

            return RedirectToAction("OrderSummary", new { cartGuid = cart.cart_guid });
        }
        
        public ActionResult OrderSummary(string cartGuid)
        {
            var cart_guid = Guid.Parse(cartGuid);
            var cart = db.carts.FirstOrDefault(x => x.cart_guid == cart_guid);

            var user = db.users.FirstOrDefault(x => x.user_guid == cart.user_guid);

            var cartItems = db.cart_items.Where(x => x.cart_guid == cart.cart_guid).ToList();

            List<person> people = new List<person>();
            foreach (var item in cartItems)
            {
                var person = db.people.FirstOrDefault(x => x.person_guid == item.person_guid);
                if (!people.Any(x => x.person_guid == person.person_guid))
                {
                    people.Add(person);
                }
            }

            var types = db.item_types.ToList();

            OrderDetails details = new OrderDetails
            {
                cart = cart,
                items = cartItems,
                user = user,
                types = types,
                people = people,
            };

            return View(details);
        }

        public void CheckCart(user user)
        {
            var cart = db.carts.FirstOrDefault(x => x.user_guid == user.user_guid && x.order_placed == false);
            if (cart != null)
            {
                var items = db.cart_items.Where(x => x.cart_guid == cart.cart_guid).ToList();
                if (items.Count > 0)
                {
                    Session["Cart"] = cart;
                }
                else
                {
                    Session["Cart"] = null;
                }
            }
            else
            {
                Session["Cart"] = null;
            }
        }
    }
}